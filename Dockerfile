FROM python:3.6.0

ARG SOURCE=/src


COPY . $SOURCE

WORKDIR $SOURCE

RUN pip install -r requirements.txt
RUN sh $SOURCE/proto_compile.sh


#!/usr/bin/env bash
if [ -d proto/grpchiggs/ ]; then
    python -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. proto/grpchiggs/*.proto
    if [ $? -eq 0 ]; then
        echo " Compile done. "
    else
        echo " Compile failed. "
    fi
else
    echo " Folder proto/grpchiggs/ not found!"
fi
"""
    Файл менеджмента.
"""
import sys
from higgs.dispatcher import higgs_dispatcher

if __name__ == "__main__":
    mode = sys.argv[1] if len(sys.argv) > 1 else None
    higgs_dispatcher(mode)

"""
Главный модуль. Старт и и настрока сервера.
"""
import asyncio

from aiohttp import web
import uvloop
import logging
from higgs.dal import common_db_init, common_db_closing
from higgs.servers import (
    async_http,
    async_grpc
)
from higgs import config
from higgs.workers import GracefulExit
from higgs.workers.asyncworker import AioHiggsFactory

log = logging.getLogger(config.APP_NAME)


def start_higgs_receiver():
    """
    Запуск главной очереди событий.
    Старт http и grpc серверов.
    """

    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    main_loop = asyncio.get_event_loop()
    db_cage = main_loop.run_until_complete(common_db_init())

    grpc_server = async_grpc.init(loop=main_loop, db_cage=db_cage)

    grpc_server.start()
    logging.getLogger(config.APP_NAME).info(
        'grpc serving on {}. Hit CTRL-C to stop.'.format(grpc_server.host_address)
    )
    http_server = async_http.init(loop=main_loop, db_cage=db_cage)
    logging.getLogger(config.APP_NAME).info(
        'Http serving on {}. Hit CTRL-C to stop.'.format(http_server['config']['host'])
    )
    try:
        web.run_app(
            http_server,
            host=http_server['config']['host'],
            port=http_server['config']['port'], loop=main_loop
        )
    except BaseException:
        log.critical('Service crashed!', exc_info=True)
    finally:
        result = main_loop.run_until_complete(common_db_closing(db_cage))
        log.info(
            'Service down. {}'.format(result)
        )


def async_higgs_worker():
    """
    Запуск асихронных обработчиков событий на отправку писем.
    """
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    main_loop = asyncio.get_event_loop()
    db_cage = main_loop.run_until_complete(common_db_init())
    courier = AioHiggsFactory(db_cage['pg_db'], db_cage['tr_qm'], loop=main_loop)
    active_bosons = courier.boson_generator()
    try:
        asyncio.ensure_future(active_bosons)
        main_loop.run_forever()
    except (GracefulExit, KeyboardInterrupt):
        log.info("Keyboard or system interrupt detected")
        courier.extinguish_bosons()
    except Exception:
        log.critical("Service crashed!", exc_info=True)
        courier.extinguish_bosons()
    finally:
        result = main_loop.run_until_complete(common_db_closing(db_cage))
        log.info("Closing resources result %s", result)
        pending = asyncio.Task.all_tasks(loop=main_loop)
        if pending:
            main_loop.run_until_complete(asyncio.gather(*pending))
        main_loop.stop()
        main_loop.close()


def higgs_dispatcher(mode='w'):
    """
    Диспетчер запуска сервиса.
    Возможен один из двух вариантов
    w - режим обработчика. выполнение рассылки.
    r - режим приёмника задач. принимает запросы на создание задач по рассылке.
    """
    log.info('Get mode key %s', mode)
    if mode == 'w':
        log.info('higgs start in worker mode.')
        async_higgs_worker()
    elif mode == 'r':
        log.info('higgs start in receiver mode.')
        start_higgs_receiver()

    else:
        log.info(
            'Unknown higgs mode please chose some one: w - workers mode; r - receiver mode.'
        )

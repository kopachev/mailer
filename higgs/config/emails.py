"""
Настройки почтового сервиса
"""
from os import getenv

# Количество одновременных обработчиков очереди писем.
EMAIL_WORKERS_COUNT = int(getenv('EMAIL_WORKERS_COUNT', 2))
"""
Конфигурация почтового севрвера.
"""
# Основные параметры
EMAIL_HOST = getenv('EMAIL_HOST', 'localhost')
EMAIL_PORT = int(getenv('EMAIL_PORT', 1025))
EMAIL_HOST_USER = getenv('EMAIL_HOST_USER', None)
EMAIL_HOST_PASSWORD = getenv('EMAIL_HOST_PASSWORD', None)

# Тип шифрования канала. Требуется только один параметр. Если указать оба возникнет ошибка.
EMAIL_USE_TLS = getenv('EMAIL_USE_TLS', 'FALSE').upper() == 'TRUE'
EMAIL_USE_STARTTLS = getenv('EMAIL_USE_STARTTLS', 'FALSE').upper() == 'TRUE'
"""
Количество попыток переподключения при неудачной попытке установить соединение с почтовым сервисом.
Время между попытками вычисляется по формуле 2*номер попытки*10 секунд.
"""
EMAIL_RECONNECT_RETRIES = int(getenv('EMAIL_RECONNECT_RETRIES', 5))
# Время ожидания ответа от сокета почтового сервера при попытке соединения.
EMAIL_TIMEOUT = int(getenv('EMAIL_TIMEOUT', 60))
# Пути к файлам сертификатов(Функционал не тестирован).
EMAIL_KEYFILE = None
EMAIL_CERTFILE = None

# Использовать в заголовке Date локальное время или UTC.
EMAIL_USE_LOCALTIME = None
# Значение заголовка From.
EMAIL_DEFAULT_FROM_EMAIL = 'admin@local.loc' if EMAIL_HOST_USER is None else EMAIL_HOST_USER

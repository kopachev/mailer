"""
Основные настройки приложения.
"""
from os import getenv
import os
import socket

# Иморт конфигураций баз данных
from .database import (  # NOQA
    POSTGRES,
    TARANTOOL,
    REDIS
)
# Импорт настроек почтовой рассылки
from .emails import (  # NOQA
    EMAIL_WORKERS_COUNT,
    EMAIL_HOST,
    EMAIL_PORT,
    EMAIL_HOST_USER,
    EMAIL_HOST_PASSWORD,
    EMAIL_USE_TLS,
    EMAIL_USE_STARTTLS,
    EMAIL_RECONNECT_RETRIES,
    EMAIL_TIMEOUT,
    EMAIL_KEYFILE,
    EMAIL_CERTFILE,
    EMAIL_USE_LOCALTIME,
    EMAIL_DEFAULT_FROM_EMAIL,
)

# Название приложения
APP_NAME = "Higgs"
# Работа в режие дебага.
DEBUG = "TRUE" == getenv("APP_DEBUG", "FALSE").upper()
# Секретный ключ приложения.
SECRET_KEY = getenv('APP_SECRET_KEY', 'you-will-never-guess-mi'),
# HTTP адрес приложения
HTTP_HOST = getenv('HTTP_APP_HOST', '127.0.0.1')
# HTTP порт сервера приложения
HTTP_PORT = getenv('HTTP_APP_PORT', 8881)
# HTTP адрес приложения
GRPC_HOST = getenv('GRPC_APP_HOST', '127.0.0.1')
# HTTP порт сервера приложения
GRPC_PORT = getenv('GRPC_APP_PORT', 50053)
# Сентри DSN
SENTRY_DSN = getenv('SENTRY_DSN', '')
# Абслоютный путь к папке приложения
APP_FOLDER = os.getcwd()
# Абсолюный путь к шаблонам писем
MAIL_TEMPLATES_FOLDER = os.path.join(APP_FOLDER, 'higgs', 'static', 'templates')
# Допустимые домены
ALLOW_DOMAINS = getenv('ALLOW_DOMAINS', '*')
# Кодировака приложения
DEFAULT_CHARSET = 'utf-8'
# Домен приложения
DNS_NAME = getenv('DNS_NAME', socket.getfqdn())

http_dict_config = {
    'app_name': APP_NAME,
    'debug': DEBUG,
    'host': HTTP_HOST,
    'port': HTTP_PORT,
}

grpc_dict_config = {
    'host': GRPC_HOST,
    'port': GRPC_PORT,
}

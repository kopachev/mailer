"""
Настройки логгирования
"""

# Расширенный формат сообщей.
import logging

from . import APP_NAME, DEBUG, SENTRY_DSN

root_format = """%(levelname)s|%(asctime)s|%(filename)s|%(module)s|%(lineno)d; message:%(message)s"""


class AsynctntWarnFilter(logging.Filter):
    """
    Фильтра для отсекания сообщений от библиотеки тарантула о не соответсвии формата обьявления.
    Пример сообщений
    |asynctnt|WARNING "Field #0 of space _queue is not in space format definition"
    |asynctnt|WARNING "Field #1 of space _queue is not in space format definition"
    |asynctnt|WARNING "Field #0 of space _queue_consumers is not in space format definition"
    |asynctnt|WARNING "Field #1 of space _queue_consumers is not in space format definition"
    |asynctnt|WARNING "Field #2 of space _queue_consumers is not in space format definition"
    |asynctnt|WARNING "Field #3 of space _queue_consumers is not in space format definition"
    |asynctnt|WARNING "Field #0 of space _queue_taken is not in space format definition"
    |asynctnt|WARNING "Field #1 of space _queue_taken is not in space format definition"
    |asynctnt|WARNING "Field #2 of space _queue_taken is not in space format definition"
    |asynctnt|WARNING "Field #1 of space _queue_taken is not in space format definition"
    |asynctnt|WARNING "Field #2 of space _queue_taken is not in space format definition"
    |asynctnt|WARNING "Field #0 of space notification_mail is not in space format definition"
    |asynctnt|WARNING "Field #1 of space notification_mail is not in space format definition"
    |asynctnt|WARNING "Field #5 of space notification_mail is not in space format definition"
    |asynctnt|WARNING "Field #0 of space notification_mail is not in space format definition"
    |asynctnt|WARNING "Field #1 of space notification_mail is not in space format definition"
    |asynctnt|WARNING "Field #2 of space notification_mail is not in space format definition"
    """

    def filter(self, record):
        """
        Отфильтровует записи в которых содержиться фраза is not in space format definition
        """
        if record.msg is not None:
            return not ('is not in space format definition' in record.msg)
        return True


# Полная конфигурация логгинга
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '|%(name)s|%(levelname)s[%(asctime)s]|%(filename)s|%(module)s|%(lineno)d: "%(message)s"',
            'datefmt': "%H:%M:%S %d.%m.%Y",
        },
        'info': {
            'format': '%(levelname)s:[%(asctime)s]:"%(message)s"',
            'datefmt': "%H:%M:%S %d.%m.%Y",
        }
    },
    'handlers': {
        'debug_console': {
            'level': 'DEBUG',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
        'info_console': {
            'level': 'INFO',
            'formatter': 'info',
            'class': 'logging.StreamHandler',
        },
        'warning_console': {
            'level': 'WARNING',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
        'err_console': {
            'level': 'ERROR',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
        'sentry_warn': {
            'level': 'WARNING',
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': SENTRY_DSN,
            'auto_log_stacks': True,
            'ignore_exceptions': ['KeyboardInterrupt']
        },
        'sentry_error': {
            'level': 'ERROR',
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': SENTRY_DSN,
            'auto_log_stacks': True,
            'ignore_exceptions': ['KeyboardInterrupt']
        }
    },
    'filters': {
        'asynctwarn': {
            '()': AsynctntWarnFilter,
        }

    },
    'loggers': {
        '': {
            'handlers': ['debug_console'] if DEBUG else ['warning_console', 'sentry_error'],
            'level': 'DEBUG' if DEBUG else 'WARNING',
        },
        APP_NAME: {
            'handlers': ['debug_console'] if DEBUG else ['info_console', 'sentry_warn'],
            'level': 'DEBUG' if DEBUG else 'INFO',
            'propagate': False
        },
        'asynctnt': {
            'handlers': ['debug_console'] if DEBUG else ['info_console', 'sentry_warn'],
            'level': 'DEBUG' if DEBUG else 'INFO',
            'filters': ['asynctwarn'],
            'propagate': False
        },

        'sentry.errors': {
            'handlers': ['err_console'],
            'level': 'ERROR',
            'propagate': False
        },
        'sentry.errors.uncaught': {
            'handlers': ['err_console'],
            'level': 'ERROR',
            'propagate': False
        }
    }
}

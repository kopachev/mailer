"""
Настройки баз данных.
"""
from os import getenv

# База данных пользователей. СУБД PostgreSQl.
POSTGRES = {
    'host': getenv('ME_POSTGRES_HOST', '127.0.0.1'),
    'port': int(getenv('ME_POSTGRES_PORT', 5432)),
    'user': getenv('ME_POSTGRES_USER', 'auth'),
    'password': getenv('ME_POSTGRES_PASS', 'auth'),
    'database': getenv('ME_POSTGRES_DB', 'auth')
}


# Сервер очередей TARANTOOL.
NOTIFICATIONS_TUBE = 'notification_mail'

TARANTOOL = {
    'config': {
        'host': getenv('ME_TARANTOOL_HOST', '127.0.0.1'),
        'port': int(getenv('ME_TARANTOOL_PORT', 3301)),
        'username': getenv('ME_TARANTOOL_USER', 'local'),
        'password': getenv('ME_TARANTOOL_PASS', 'local'),
        'reconnect_timeout': 5
    },
    'tubes': {
        NOTIFICATIONS_TUBE: getenv('ME_TARANTOOL_NOTIFICATION_MAIL_TUBE', 'notification_mail')
    },
    'spaces': {}
}


# База горячих данных. Хранилище REDIS.
REDIS = {
    'host': getenv('ME_REDIS_HOST', '127.0.0.1'),
    'port': int(getenv('ME_REDIS_PORT', 6379)),
    'password': getenv('ME_REDIS_PASS', 'local'),
    'db': int(getenv('ME_REDIS_DB', '5'))
}

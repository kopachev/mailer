"""
Промежуточные обработчки.
"""

import json
import logging

from aiohttp import web

from higgs.config import APP_NAME, ALLOW_DOMAINS

log = logging.getLogger(APP_NAME + '.middleware')


async def handle_500():
    """
    Обертка для возвращения внутренней ошибки сервера
    """
    body = json.dumps({'error': 'Internal server Error'}).encode('utf-8')
    response = web.Response(body=body, status=500)
    return response


def handle_400(err):
    """
    Обертка для возвращения ответа о плохих входящих данных.
    """
    details = {
        'error': 'Bad request'
    }
    if getattr(err, 'text', None) is not None:
        details['reason'] = err.text
    if getattr(err, 'extra', None) is not None:
        details['extra'] = err.extra
    body = json.dumps(details).encode('utf-8')
    response = web.Response(body=body, status=400)
    return response


def handle_404(reason=None):
    """
    Обертка для возвращения ответа о не найденном ресурсе.
    """
    details = {
        '404': 'Not found'
    }
    if reason is not None:
        details['reason'] = reason
    body = json.dumps(details).encode('utf-8')
    response = web.Response(body=body, status=404)
    return response


async def headers_middleware(app, handler):
    """
    Обработка загололовков.
    """
    async def middleware_handler(request):
        """
        Промежуточный обработчик. Выполняет менеджмент хедеров запроса и ответа.
        Выводин в консоль информационные сообщения о поступивших запросах.
        """
        ip_address = request.headers.get('X-Forwarded-For', None)
        if ip_address is None:
            ip_address = request.transport.get_extra_info('peername', (None, None))[0]  # ('127.0.0.1', 57916)
        request.headers['requesting_ip'] = ip_address
        destination = request.path
        method = request.method
        user_agent = request.headers.get('User-Agent')
        log.info(
            'Received request from: %s, dst: %s, method: %s, details: %s', ip_address, destination, method, user_agent
        )
        accept = request.headers.get('Accept')
        if accept is None or 'application/json' not in accept:
            if 'text/html' not in accept:
                accept = 'text/plain'
        response = await handler(request)
        if response.headers.get('Access-Control-Allow-Methods', None) is None:
            response.headers['Access-Control-Allow-Methods'] = 'OPTIONS, HEAD'
        response.headers['Access-Control-Allow-Origin'] = ' '.join(ALLOW_DOMAINS) \
            if isinstance(ALLOW_DOMAINS, list) else ALLOW_DOMAINS
        response.headers['Access-Control-Allow-Headers'] = 'Accept, Content-Type'
        response.headers['Accept'] = 'application/json'
        response.headers['Accept-Charset'] = 'utf-8'
        response.headers['Content-Language'] = 'en'
        response.headers['Server'] = 'Notification web-hook'
        if response.text:
            response.content_type = accept
        return response

    return middleware_handler


async def errors_middleware(app, handler):
    """
    Глобальный перехват ошибок всех обработчиков
    """
    async def middleware_handler(request):
        """
        Промежуточный обработчик
        """
        try:
            response = await handler(request)
        except web.HTTPBadRequest as e:
            log.debug(e.reason)
            return handle_400(e)
        except web.HTTPClientError as e:
            log.warning('Bad request is provided. details: %s', e.text)
            return web.json_response({'error': e.text}, status=e.status)
        except Exception as e:
            log.exception(
                'Handler exception'
            )
            return await handle_500()
        else:
            return response

    return middleware_handler

"""
    Инициализация приложения.
"""

from logging.config import dictConfig

from .config import logging_config

dictConfig(logging_config.LOGGING)

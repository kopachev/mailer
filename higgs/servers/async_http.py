"""
http сервер на базе aiohttp
"""
from aiohttp import web

from higgs import config


from higgs.middleware import headers_middleware, errors_middleware
from higgs.routes import setup_routes


def init(loop=None, db_cage=None):
    """
    Инициализация сервера
    """
    app = web.Application(loop=loop)

    app['config'] = config.http_dict_config
    if db_cage is not None:
        app['pg_db'] = db_cage.get('pg_db', None)
        app['tr_qm'] = db_cage.get('tr_qm', None)
        app['redis'] = db_cage.get('redis', None)
    app.middlewares.extend((headers_middleware, errors_middleware))

    setup_routes(app)
    return app

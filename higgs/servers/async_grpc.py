"""
https://gist.github.com/seglberg/0b4487b57b4fd425c56ad72aba9971be
"""
from higgs import config
from .utils import AsyncioExecutor
from higgs.handlers import alter
import grpc
from proto.grpchiggs import srv_pb2_grpc

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


def init(loop=None, db_cage=None):
    """
    Инициализация grpc сервера.
    """
    server_config = '{host}:{port}'.format(**config.grpc_dict_config)
    server = grpc.server(AsyncioExecutor(loop=loop))
    higgs_handlers = alter.Higgs(db_cage)
    srv_pb2_grpc.add_HiggsServicer_to_server(higgs_handlers, server)
    server.add_insecure_port(server_config)
    server.host_address = config.grpc_dict_config['host']
    return server

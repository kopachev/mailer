"""
Обработчик очереди рассылки
Асинхронная версия.
"""

import asyncio
import logging
import random
import signal
import threading
from asyncio import Semaphore

from higgs.config import APP_NAME, EMAIL_WORKERS_COUNT
from higgs.workers import GracefulExit
from higgs.workers.mail.asyncsmtp import AsyncSmtpManager
from higgs.workers.mail.message_factory import EmailFactory

log = logging.getLogger(APP_NAME + '.async.task_worker')


def raise_graceful_exit():
    """
    Обработчик системного сигнала на завершение скрипта.
    """
    raise GracefulExit()


class WorkerCounterSemaphore(Semaphore):
    """
    Класс семафора который позволяет узнать количесво выполянемых задач.
    """

    def __init__(self, value=1, *, loop=None):
        super().__init__(value, loop=loop)
        self._max_workers = value

    def get_active_workers(self):
        """
        Получить количесвто активных воркеров
        """
        return self._max_workers - self._value


class AioHiggsFactory:
    """
    Класс управления отправкой почты.
    """

    def __init__(self, pg_db, tr_qm, loop=None):
        own_loop = False
        if loop is None:
            loop = asyncio.get_event_loop()
            own_loop = True
        loop.add_signal_handler(signal.SIGINT, raise_graceful_exit)
        loop.add_signal_handler(signal.SIGTERM, raise_graceful_exit)
        self._own_loop = own_loop
        self._loop = loop
        self._tr_qm = tr_qm
        tube_statistic = self._loop.run_until_complete(self._tr_qm.get_notification_tube_statistic())
        log.info(
            'Notification tube statistic; \n %s', tube_statistic
        )
        self._worker_count = EMAIL_WORKERS_COUNT
        self._smtp_manager = AsyncSmtpManager  # берёт всю конфигурацию из higgs.config.emails
        self._mail_factory = EmailFactory(pg_db)
        self._hard_workers = None
        self._do_spin = True
        self._bar_lock = threading.Lock()

    async def _queue_listening(self, worker_name=None):
        """
        Метод для прослушивания очереди.
        Через некоторый период времени происходи проверка флага выпонения.
        Если флаг выполения self._do_spin False, то прослушивание прерывается и возвращается таск(может быть None),
        либо None.
        """
        print_status_counter = 1
        while 1:
            if not self._do_spin:
                break
            try:
                if print_status_counter > 10:
                    log.info('Worker %s listen queue.', worker_name)
                    print_status_counter = 0
                print_status_counter += 1
                listen_time = random.randint(10, 15)
                task = await self._tr_qm.get_notification_task(timeout=listen_time)
            except self._tr_qm.task_empty_exc:
                continue
            return task
        return None

    async def boson(self, worker):
        """
        В методе происходит получение таска. Если таск None,
        предполагается, что при получении такса произошла вненштаная ситуация.
        В этом случае выпониться проверка флага выпонения процеса self._do_spin. Если флаг True,
        будет предпринята попытка снова получить корректный таск.
        В лучае очереди неудач, каждая следующая попытка будет предприниматься с нарастающим периодом времени.
        Маскимальный период ожидания между попытками 300 сек.
        """
        fuckups = 0
        while 1:
            log.info("Worker %s listen task queue.", worker)
            task = await self._queue_listening(worker_name=worker)
            if task is None:
                if not self._do_spin:
                    break
                max_wait_retry = 300
                wait_retry = 10 * fuckups
                wait_retry = wait_retry if wait_retry < max_wait_retry else max_wait_retry
                fuckups += 1
                log.error('Exception in tarantool db service. Retry after %s seconds.', wait_retry)
                await asyncio.sleep(wait_retry)
                continue
            fuckups = 0
            task_data = task.data
            email_type = task_data.get('type', 'EMPTY')
            recipients = task_data.get('emails', [])
            lang = task_data.get('lang', 'ENG')
            msg = 'worker %s, take task id %s,(type: %s, recipients count: %s, lang: %s), processing...'
            log.info(msg, worker, task.task_id, email_type, len(recipients), lang)
            if len(recipients) < 1:
                log.warning('Detect wrong task, No recipients found.')
                task = await self._tr_qm.bury_task(task)
                log.info('Task %s buried.', task)
                continue
            emails = await self._mail_factory.create_emails(recipients, email_type, lang)
            if emails is not None:
                num_sent_mails, failed = await self._smtp_manager(worker=worker).send_emails(emails)
                if num_sent_mails == len(recipients):
                    task = await self._tr_qm.ack_task(task)
                else:
                    log.error('Not all recipients email sent. Required: %s, Failed %s', recipients, failed)
                    task = await self._tr_qm.bury_task(task)
                    log.info('Task %s buried.', task)
                log.debug('worker %s , task %s, completed.', worker, task)
            else:
                log.warning('No mails was created. task %s', task)
        log.info('Worker %s stopped', worker)

    async def boson_generator(self):
        """
        Генератор обработчиков. Количество одновременных обработчиков задается в настройках.
        """
        log.info(
            'Higgs workers count: %s.', self._worker_count
        )
        active_boson = [self.boson(i + 1) for i in range(self._worker_count)]
        active_boson = asyncio.gather(*active_boson)

        return active_boson

    # def boson_generator(self):
    #         """
    #         Генератор обработчиков. Количество одновременных обработчиков задается в настройках.
    #         """
    #         log.info(
    #             'Higgs workers count: %s.', self._worker_count
    #         )
    #         active_boson = [self.boson(i + 1) for i in range(self._worker_count)]
    #         active_boson = asyncio.gather(*active_boson, return_exceptions=False)
    #         return active_boson

    def extinguish_bosons(self):
        """
        Остановить обработчики. Метод выставляет флаг, благодаря которому воркеры плавно остановят свои циклы.
        """
        log.info('Stop signal detected!')
        self._do_spin = False
        pending = asyncio.Task.all_tasks(loop=self._loop)
        if pending:
            results = self._loop.run_until_complete(asyncio.gather(*pending, return_exceptions=False))
            # results = self._loop.run_until_complete(asyncio.gather(*pending))
            log.info('Waited process stop result: %s', results)
        tube_statistic = self._loop.run_until_complete(self._tr_qm.get_notification_tube_statistic())
        log.info(
            'Workers stopped. Notification tube statistic; \n %s', tube_statistic
        )

"""
Модуль воркера
"""


class TaskDataError(Exception):
    """
    Ошибка при проверка данных задачи
    """
    pass


class GracefulExit(SystemExit):
    """
    Для корректного выхода из воркера
    """
    code = 1

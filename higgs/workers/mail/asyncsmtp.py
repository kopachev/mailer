"""
Реализация клиента SMTP
"""
from collections import namedtuple

import aiosmtplib
import threading
import logging


import asyncio

import tqdm

import higgs.config as settings

log = logging.getLogger(settings.APP_NAME + '.smtpmanager')


class AsyncSmtpManager:
    """
    Класс оберка для управления SMTP подключением.
    """

    def __init__(self, worker=None):

        self.username = settings.EMAIL_HOST_USER
        self.password = settings.EMAIL_HOST_PASSWORD

        self.use_tls = settings.EMAIL_USE_TLS
        self.use_starttls = settings.EMAIL_USE_STARTTLS

        # # TODO возможно оповещения перенести в настройки
        # if not settings.EMAIL_USE_TLS and not settings.EMAIL_USE_STARTTLS:
        #     log.warning('No one TLS, or STARTTLS not enable! Not secure connection will used!')

        if settings.EMAIL_USE_TLS and settings.EMAIL_USE_STARTTLS:
            log.warning('Enabled TLS, and STARTTLS option!,TLS will be used TLS')
            self.use_starttls = False

        self.client = aiosmtplib.SMTP(
            hostname=settings.EMAIL_HOST,
            port=settings.EMAIL_PORT,
            timeout=settings.EMAIL_TIMEOUT,
            source_address=settings.DNS_NAME,
            use_tls=settings.EMAIL_USE_TLS,
            client_key=settings.EMAIL_KEYFILE,  # str
            client_cert=settings.EMAIL_CERTFILE,  # str
            validate_certs=True,
            tls_context=None,  # ssl.SSLContext
            cert_bundle=None,  # str
            loop=None  # EventLoop
        )
        self._control_worker = worker
        self._lock = threading.RLock()
        self.connect_retries = settings.EMAIL_RECONNECT_RETRIES

    async def _init_connection(self):
        """
        Метод установки соединения с сервером.
        Пробует соеденитьсяс серверром заданное количесво раз (указано в параметре self.connect_retries)
        В случае не успеха возвращает None.
        """
        conn_status = aiosmtplib.SMTPStatus.closing
        for i in range(self.connect_retries):
            try:
                log.debug('Try to connect %s:%s.', self.client.hostname, self.client.port)
                response = await self.client.connect()
                if response.code != aiosmtplib.SMTPStatus.ready:
                    log.warning(
                        'Server connection is established. But status code is not READY(220), response: %s', response
                    )
                if self.use_starttls:
                    response = await self.client.starttls()
            except (
                    aiosmtplib.errors.SMTPConnectError,
                    aiosmtplib.errors.SMTPTimeoutError,
                    aiosmtplib.errors.SMTPServerDisconnected,
            ) as ex:
                msg = 'SMTP connection to %s:%s retry unsuccessful. exception: %s: %s'
                log.error(
                    msg, self.client.hostname, self.client.port, type(ex), ex
                )
                self.client.close()
                conn_status = aiosmtplib.SMTPStatus.closing
                log.debug(
                    '%s from %s retries left. ', self.connect_retries - (i + 1), self.connect_retries
                )
                is_reconnect = (i < self.connect_retries - 1)
                if is_reconnect:
                    time_to_reconnect = (i + 1) * 2
                    # time_to_reconnect = (i + i) * 10
                    log.debug('Wait %s seconds before reconnect. ', time_to_reconnect)
                    await asyncio.sleep(time_to_reconnect)
                    log.debug('Reconnecting...')
                else:
                    log.error('Connection to %s:%s failed!', self.client.hostname, self.client.port)
            except aiosmtplib.errors.SMTPException as ex:
                msg = 'SMTP connection to %s:%s failed!. exception: %s: %s'
                log.error(
                    msg, self.client.hostname, self.client.port, type(ex), ex
                )
                conn_status = aiosmtplib.SMTPStatus.closing
                self.client.close()
                break
            else:
                conn_status = aiosmtplib.SMTPStatus.ready
                msg = 'Connection with server is established. Response: %s, connection status set %s'
                log.debug(msg, response, conn_status)
                break
        return conn_status

    async def _server_authorization(self):
        """
        Метод получения авторизированного доступа на почтовый сервер.
        Если предоставляются авторизационные данные метод предпринимает одну попытку аторизоваться.
        В случае ответа auth_successful = 235, метод возвращает ответ завершенной операции.
        Если авторизационные данные не предоставленны, то метод возвращает статус завершенной операции.
        Любые другие ответы следует рассматривать как ошибку.
        В лучае предоставления учетных данных и не возможности получить ответ со статусом 235,
        метод закрывает соединение.
        """
        auth_status = aiosmtplib.SMTPStatus.completed
        if self.username and self.password:
            log.debug(
                'SMTP credential is provided. username: %s, password: %s. Try to auth...', self.username, self.password
            )
            try:
                response = await self.client.login(username=self.username, password=self.password)
            except aiosmtplib.errors.SMTPAuthenticationError as ex:
                log.error('%s', ex.message)
                auth_status = aiosmtplib.SMTPStatus.auth_failed
                self.client.close()
            except aiosmtplib.errors.SMTPException as ex:
                auth_status = aiosmtplib.SMTPStatus.auth_failed
                log.error('%s', ex.message)
                self.client.close()
            else:
                if response.code != aiosmtplib.SMTPStatus.auth_successful:
                    auth_status = aiosmtplib.SMTPStatus.auth_failed
                    log.error('Auth failed, server response %s, auth status set', response, auth_status)
                    self.client.close()
                else:
                    auth_status = aiosmtplib.SMTPStatus.completed
                    log.debug('Response auth from server %s auth status set %s', response, auth_status)
        else:
            log.debug('SMTP credential is not provided.')
            auth_status = aiosmtplib.SMTPStatus.completed
        return auth_status

    async def open(self):
        """
        Метод для регулярного открытия соединения перед отправкой письма.
        В случает плохих ответов от сервера метод возвращает None
        """
        init_connection_status = await self._init_connection()
        if init_connection_status != aiosmtplib.SMTPStatus.ready:
            return None
        authorization_status = await self._server_authorization()
        if authorization_status is None or authorization_status != aiosmtplib.SMTPStatus.completed:
            return None
        return init_connection_status

    async def check_mail_server(self):
        """
        Метод для стартовой проверки состояния и параметров SMTP сервера.
        Проверяется доступность и возможность совершить авторизацию.
        В случае плохих отвертов от сервера метод возвращает None.
        """
        init_connection_status = await self._init_connection()
        if init_connection_status != aiosmtplib.SMTPStatus.ready:
            return None
        server_status_response = await self.client.ehlo()
        log.info('Server status response %s', server_status_response)
        if self.username and self.password:
            log.info(
                'SMTP credential is provided. username: %s, password: %s. Try to auth...', self.username, self.password
            )
            status = await self._server_authorization()
            if status != aiosmtplib.SMTPStatus.completed:
                log.error('Credential provided. authorisation is required. authorisation is failed.')
                return None
        await self.client.quit()
        return server_status_response

    async def _send_message(self, message):
        """
        Метод выполняет отправку одного объекта message.
        Отправка нескольких обьектов через одно соединение выполняется синхронно
        """
        SndRes = namedtuple('SndRes', 'recipient is_sent')
        result = None
        rec = message.get_all('To', 'No Recipients')
        try:
            log.debug('Sending message to %s', rec)
            response = await self.client.send_message(message)
        except ValueError as ex:
            err_msg = ex.args
            log.error(
                'Bad message. Message Not Sent to recipient %s. exception %s', rec, err_msg
            )
            result = SndRes(rec, False)
        except (aiosmtplib.SMTPResponseException, aiosmtplib.SMTPRecipientsRefused):
            rec = message.get_all('To', 'No Recipients')
            log.exception('In sending message to %s. Try to sending messages continue', rec)
            result = SndRes(rec, False)
        except aiosmtplib.SMTPException:
            log.exception('Dispatching emails was crashed')
            result = SndRes(rec, False)
        except Exception:
            log.critical('Dispatching emails was crashed', exc_info=True)
            result = SndRes(rec, False)
        else:
            log.debug('Message send on server to recipient %s, sending result %s', rec, response)
            #  TODO изучить возможность неуспешных ответов сервера.
            if 'response is bad':
                pass  # Заглушка
            result = SndRes(rec, True)
        return result

    async def send_emails(self, messages):
        """
        Метод выполняет отправку сформированных писем.
        """
        num_sent = 0
        if not messages:
            log.warning('Messages for sending not provided')
            return num_sent
        is_open = await self.open()
        if is_open is None:
            log.debug('Connection not opened. Messages not sent')
            return num_sent
        count_messages = len(messages)
        log.debug('Connection opened %s messages provided to dispatching', count_messages)
        tasks = [asyncio.ensure_future(self._send_message(message)) for message in messages]
        to_do_iter = asyncio.as_completed(tasks)
        # TODO проблемы спрогресс баром. Если больше 2 воркеров появляются лишние строки
        if settings.EMAIL_WORKERS_COUNT < 4:
            to_do_iter = tqdm.tqdm(
                to_do_iter,
                total=len(messages),
                desc='Worker {} progress'.format(self._control_worker)
            )
        failed = []
        for future in to_do_iter:
            result = await future
            if not result.is_sent:
                failed.extend(result.recipient)
        # done, _ = await asyncio.wait(tasks)
        # failed = [task.result().recipient for task in done if not task.result().is_sent]
        try:
            result = await self.client.quit()
        except (aiosmtplib.SMTPResponseException, aiosmtplib.SMTPException):
            log.error('Closing connection is crashed')
        else:
            log.debug('Connection to server closed, result %s', result)
        num_sent = count_messages - len(failed)
        log.debug('%s was sent, failed %s ', num_sent, failed)
        return num_sent, failed

"""
Модуль формирования элеткроных писем.
"""

import logging

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate, make_msgid

from higgs import config

log = logging.getLogger(config.APP_NAME + '.email_factory')


class EmailFactory:
    """
    Класс создания электонных писем
    """

    def __init__(self, db_manager):
        self.storage_manager = db_manager
        self.from_email = config.EMAIL_DEFAULT_FROM_EMAIL
        self.charset = config.DEFAULT_CHARSET

    def create_message(self, recipient, email_template):
        """
        Создание обьекта message по предоставленному шаблону.
        """
        alternative_subtype = 'alternative'
        log.debug('Creating email of %s  type for recipient %s ', email_template.get('title', None), recipient)
        msg = MIMEMultipart(_subtype=alternative_subtype, _charset=self.charset)
        text_msg = MIMEText(email_template.get('text_body', ''), _subtype='plain', _charset=self.charset)
        msg.attach(text_msg)
        html_msg = MIMEText(email_template.get('html_body', ''), _subtype='html', _charset=self.charset)
        if email_template.get('html_body', ''):
            msg.attach(html_msg)
        else:
            msg = text_msg
        msg['Subject'] = email_template.get('subject', '')
        msg['From'] = self.from_email
        msg['To'] = recipient
        msg['Date'] = formatdate(localtime=config.EMAIL_USE_LOCALTIME)
        msg['Message-ID'] = make_msgid(domain=config.DNS_NAME)
        log.debug('Email on %s lang for %s created', email_template.get('lang', None), recipient)
        return msg

    async def create_emails(self, recipients, template_type, lang):
        """
        Создание писем для рассылки в соответсвии с указанным типом шаблона и локализацией.
        """
        if not isinstance(recipients, list) or not len(recipients):
            log.warning('Recipients not found, cant create mails')
            return None
        log.debug('Creating mass email dispatch. Get template for %s type on %s lang', template_type, lang)
        email_template = await self.storage_manager.get_mail_content(template_type, lang)
        if email_template is None:
            log.warning('Email template not found, cant create mails')
            return None
        emails = [self.create_message(recipient, email_template) for recipient in recipients]
        return emails

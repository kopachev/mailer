"""
Обработчики изменяющие состояние данных.
"""
import json
import logging
import random
import re

from aiohttp import web

from higgs.captcha_maker.captcha.image import make_captcha
from higgs.config import APP_NAME
from higgs.constants import Language
from proto.grpchiggs import srv_pb2, messages_pb2

log = logging.getLogger(APP_NAME + '.handlers.alter')


async def captcha_maker(request):
    """
    Создание каптчи. Управляется созданием и занесением данных каптчи в БД.
    """
    captcha, value = make_captcha()
    captcha_token = await request.app['redis'].save_new_value_random_key(value)
    log.debug(
        'Produce new captcha value: %s, token: %s', value, captcha_token
    )
    return {
        'captcha_token': captcha_token,
        'captcha': captcha
    }


async def common_request_json_validator(request):
    """
    Общая валидация запроса. Проверяется корректность типа данных и тела запроса.
    Возвращает словарь данных запроса.
    """
    if not request.has_body:
        raise web.HTTPBadRequest(text='no content')
    content_type = request.headers.get('Content-type')
    if content_type is None or content_type.lower() != 'application/json':
        raise web.HTTPBadRequest(text='bad content type')
    try:
        request_dict = await request.json()
    except json.JSONDecodeError:
        raise web.HTTPBadRequest(text='bad content')
    return request_dict


async def validate_captcha(request, request_dict):
    """
    Проверка каптчи.
    Проверяет значение присланное клиентом.
    В случае не соответсвия значения хранящегося в БД кидает ошибку.
    """
    captcha_token = request_dict.get('captcha_token', None)
    captcha_solution = request_dict.get('captcha_solution', None)
    if captcha_token is None or captcha_solution is None:
        raise web.HTTPBadRequest(text='bad captcha is provided')
    captcha_value = await request.app['redis'].get_delete_value(captcha_token)
    if captcha_solution != captcha_value:
        raise web.HTTPBadRequest(text='bad captcha solution is provided')
    return True


async def email_request_isvalid(request):
    """
    Логика обработки валидных данных зароса на добавление элетронного адреса.
    В теле запроса должен быть json содержащий поле 'email'.
    Адрес почты должен пройти проверку на корректность.
    В случае не выполнения одного из условий валидации возбуждается исключение.
    Возвращает емейл и ip адресс откуда пришел запрос.
    """
    content_language = request.headers.get('Content-language')
    try:
        if content_language is not None:
            lang = Language[content_language.upper()]
        else:
            raise KeyError
    except KeyError:
        raise web.HTTPBadRequest(text='bad content language')
    request_dict = await common_request_json_validator(request)
    client_email = request_dict.get('email', None)
    if client_email is None:
        raise web.HTTPBadRequest(text='email not provided')
    if not re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", client_email):
        raise web.HTTPBadRequest(text='bad email is provided')
    await validate_captcha(request, request_dict)
    ip_address = request.headers.get('requesting_ip', None)
    return client_email, ip_address, lang


async def email_register_handler(request):
    """
    Обработчик регистрации емейлов.
    Записыает в базу данных адрес электронной почты и ip адрес, с которого пришло письмо.
    В случае успешного добавления создает задачу на отправку письма на указанный адрес электронной почты.
    """
    try:
        client_email, ip_address, language = await email_request_isvalid(request)
        result = await request.app['pg_db'].add_subscriber_mail(
            client_email,
            ip_address=ip_address,
            language=language.value
        )
    except request.app['pg_db'].DUPLICATE_DATA as dbe:
        e = web.HTTPBadRequest(text='Email already exist')
        e.extra = await captcha_maker(request)
        raise e
    except web.HTTPBadRequest as e:
        e.extra = await captcha_maker(request)
        raise
    dispatch_task = {
        'type': 'GREETY',
        'emails': [client_email, ],
        'lang': language.value
    }
    try:
        task = await request.app['tr_qm'].make_notification_mail_task(dispatch_task, ttr=350)
    except Exception:
        log.exception('Can`t create task for sending mail, client email %s', client_email)
        # Не зависимо от того удалось или нет создать задачу на рассылку запись емейла в базу производиться.
        # raise
    else:
        log.debug('Dispatch task created, task: %s', task)
    headers = {
        'Access-Control-Allow-Methods': 'POST, OPTIONS, HEAD',
    }
    body = json.dumps(result, ensure_ascii=False)
    return web.HTTPOk(text=body, headers=headers)


async def captcha_distributor(request):
    """
    Выдавание каптчи. Выдает каптчу по запросу.
    """
    headers = {
        'Access-Control-Allow-Methods': 'GET, OPTIONS, HEAD',
    }
    body = json.dumps(await captcha_maker(request), ensure_ascii=False)
    return web.HTTPOk(text=body, headers=headers)


class Higgs(srv_pb2.HiggsServicer):
    """
    Асихронные grpc обработчики
    """

    def __init__(self, db_cage):
        if db_cage is not None:
            self.pg_db = db_cage.get('pg_db', None)
            self.tr_qm = db_cage.get('tr_qm', None)
            self.redis = db_cage.get('redis', None)

    async def GenerateHiggs(self, request, context):
        """
        Обработчик заявок по созданию задач на рассылку уведомлений.
        Метод находиться в режиме разработки. Практических функций не выполняет.
        Можно использовать для ручного создания задач в очереди.
        """
        # TODO определиться с функционалом и контекстом.
        catpcha_value = 'VERT'
        captcha_token = await self.redis.save_new_value_random_key(catpcha_value)
        captcha_value = await self.redis.get_delete_value(captcha_token)
        log.debug(
            'Receive request on create task, %s, %s', captcha_value, captcha_token
        )
        user_id = random.randint(1, 9)
        user = await self.pg_db.get_user_by_id(user_id)
        log.debug('Action USer %s', user)
        # notice = json_format.MessageToDict(request)
        # user_id = notice.get('context', {}).get('userId', None)
        # try:
        #     user_id = int(user_id) if user_id is not None else None
        # except ValueError:
        #     log.warning(
        #         'Cant parse to int user ID. user_id: ', user_id
        #     )
        #     raise
        # notice_type = notice.get('type', EmailType.NOTICE.name)

        # action = functools.partial(self.pg_db.get_user_by_id, user_id) if user_id is not None \
        #     else self.pg_db.get_all_users

        # try:
        #     addressees = await action()
        # except Exception:
        #     log.debug(
        #         'Cant get data from db'
        #     )
        #     # TODO обрабатывать или передавать дальше.
        #     raise
        # # TODO Внимание ХАРДКОД!
        es = {'kopachyov.vitaliy@yandex.ru', 'symphonyofbody@gmail.com', 'vitalik.vitalev.2018@mail.ru',
              'kopachyov.vitaliy@rambler.ru', 'wvonder@ukr.net',
              'kopachyov.vitaliy@yahoo.com'}
        rec_count = random.randint(1, 6)
        rec = list(random.sample(es, rec_count))
        lang = 'RUS' if random.randint(0, 1) > 0 else 'ENG'

        dispatch_task = {
            'type': 'GREETY',
            'emails': rec,
            'lang': lang
        }
        try:
            task = await self.tr_qm.make_notification_mail_task(dispatch_task, ttr=350)
        except Exception:
            # TODO обрабатывать или передавать дальше.
            raise
        log.debug(
            'Dispatch task created, task: %s', task
        )
        return messages_pb2.AcceptanceStatus(status="Task created. task: {}".format(task))

"""
Обработчики для извлечения данных
"""

import logging
import json

from aiohttp import web
from higgs.config import APP_NAME

log = logging.getLogger(APP_NAME + '.handlers.retrieve')


async def tube_stats(request):
    """
    Обработчик Запроса на статистику очереди NOTIFICATIONS_TUBE.
    """
    stats = await request.app['tr_qm'].get_notification_tube_statistic()
    log.debug(
        'Get tube statistics %s', stats
    )
    jstats = json.dumps(stats)
    return web.HTTPOk(content_type='application/json', text=jstats)

"""
Обработчики обшего назначения сервисные обработчки
"""
import logging

from aiohttp import web
from higgs.config import APP_NAME

log = logging.getLogger(APP_NAME + '.handlers.common')


async def server_info_handler(request):
    """
    Обработчик метода OPTIONS и HEAD для всего сервера
    """
    headers = {
        'Access-Control-Allow-Methods': 'OPTIONS, HEAD',
    }

    return web.HTTPNoContent(headers=headers)


async def regmail_info_method_handler(request):
    """
    Обработчик метода OPTIONS и HEAD для ресурса regmail
    """
    headers = {
        'Access-Control-Allow-Methods': 'POST, OPTIONS, HEAD',
    }
    return web.HTTPNoContent(headers=headers)


async def get_captcha_info_method_handler(request):
    """
    Обработчик метода OPTIONS и HEAD для ресурса captcha_distributor
    """
    headers = {
        'Access-Control-Allow-Methods': 'GET, OPTIONS, HEAD',
    }
    return web.HTTPNoContent(headers=headers)

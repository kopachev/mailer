"""
Роутер
"""
from .handlers import (
    email_register_handler,
    regmail_info_method_handler,
    server_info_handler,
    captcha_distributor,
    get_captcha_info_method_handler
)


def setup_routes(app):
    """
    Функция регистрации обработчиков http сервера
    :param app:
    """
    app.router.add_head(r'/regmail{sl:/?}', regmail_info_method_handler, name='regmail_methods')
    app.router.add_head(r'/captcha{sl:/?}', get_captcha_info_method_handler, name='captcha_distributor_methods')
    app.router.add_head('', server_info_handler, name='head_handler')

    app.router.add_post(r'/regmail{sl:/?}', email_register_handler, name='email_register')
    # app.router.add_get(r'/notification{sl:/?}', tube_stats, name='task_statistic')
    app.router.add_get(r'/captcha{sl:/?}', captcha_distributor, name='captcha_distributor')

    app.router.add_route(
        'OPTIONS', r'/regmail{sl:/?}', regmail_info_method_handler, name='regmail_options'
    )
    app.router.add_route(
        'OPTIONS', r'/captcha{sl:/?}', get_captcha_info_method_handler, name='captcha_distributor_options'
    )
    app.router.add_route(
        'OPTIONS', '', server_info_handler, name='options_method'
    )

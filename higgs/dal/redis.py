"""
Выполнение запросов к хранилищу REDIS
"""
import logging
import uuid

import aredis
from aredis import WatchError
from datetime import timedelta

from higgs.config import APP_NAME, REDIS

log = logging.getLogger(APP_NAME + ".redis_manager")


class RedisManager:
    """
    Менеджер глобального подключения к редису
    Проверят подключение при старте приложения.
    Содержит методы помогающие осуществлять запросы.
    """

    def __init__(self, pool):
        self._pool = pool

    async def save_new_value_random_key(self, value, ttl=timedelta(minutes=15)):
        """
        Добавление уникального ключа.
        Включена проверка наличия уже существующего ключа.
        Скрипт пытается сиоздать запись пока она не будет созданна.
        Может быть прерван проблемой с подключением к хранилищу.
        """
        async with await self._pool.pipeline() as pipe:
            while 1:
                try:
                    rand_key = uuid.uuid1().hex
                    await pipe.watch(rand_key)
                    if await pipe.exists(rand_key):
                        continue
                    pipe.multi()
                    await pipe.set(rand_key, value, ttl)
                    await pipe.execute()
                except WatchError:
                    continue
                else:
                    break
        return rand_key

    async def get_delete_value(self, key):
        """
        Извлечение и удаление записи из БД
        Извлекает значение по ключу и удаляет запись.
        """
        value = await self._pool.get(key)
        if value is not None:
            await self._pool.delete(key)
            return value.decode()

    @classmethod
    async def create_manager(cls):
        """
        Создание менеджера работы с редисом
        """
        pool = aredis.StrictRedis(**REDIS)
        ping = await pool.ping()
        log.info(
            'Created and checked Redis pool server %s post %s, PING response %s:', REDIS['host'], REDIS['port'], ping
        )
        return cls(pool)



INSERT INTO "MailTemplates"."Templates" (version, description) VALUES
('1.1', 'Для расылки о регистрации почты. Упрощенная разметка.');

INSERT INTO "MailTemplates"."Localization" (template, lang, subject, html_body, text_body) VALUES
(2, -- Шаблон заглушка.
'ENG', -- Название языка.
'Welcome to Messengers Insights!', -- Заголовок.
-- Тело в формате HTML.
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Messengers Insights</title>

<style>
* {
	margin:0;
	padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

body {
	-webkit-font-smoothing:antialiased;
	-webkit-text-size-adjust:none;
	width: 100%!important;
	height: 100%;
}

.container {
	display:block!important;
	max-width:400px!important;
	margin:40px auto!important;
	clear:both!important;
}

.content {
	padding:15px;
	max-width:400px;
	margin:0 auto;
	display:block;
}

p {
	margin: 40px auto;
}

</style>
</head>

<body bgcolor="#FFFFFF">
	<section class="container">

		<h3>Welcome to Messengers Insights!</h3>
		<p>
			Messengers Insights is an automated service for messengers analysis. You can observe in real time Telegram channels and chats ratings, analytics, text search in Telegram.
		</p>
		<p>
		This is an account activation email.

		</p>
		<p>

		If you did not register on mes-ins.ru then ignore this e-mail.
		</p>
		<p>

		We are completing the development of the system and we''ll send a registration e-mail and access to Messengers Insights soon.
		</p>
		<p>

		Thank you!
		</p>


		<p>Contact Info:</p>

		<div>Phone: +7 (495) 649-93-20</div>
		<div>Email: contact@mes-ins.ru</div>

	</section>

</body>
</html>',
-- Тело в формате плоского текста.
'Welcome to Messengers Insights!

Messengers Insights is an automated service for messengers analysis. You can observe dynamics, messages and views in real time.

This is an account activation email.

If you did not register on mes-ins.ru then ignore this e-mail.

To complete the registration, please confirm your intentions by clicking on the "Activate Account" button, after that you will be directed to the "Messengers Insights" site, to specify the password.

Activate my Messengers Insights account!


Contact Info:

Phone: +7 (495) 649-93-20
Email: contact@mes-ins.ru'
)

INSERT INTO "MailTemplates"."Localization" (template, lang, subject, html_body, text_body) VALUES
(2, -- Шаблон заглушка.
'RUS', -- Название языка.
'Добро пожаловать в Messengers Insights!', -- Заголовок.
-- Тело в формате HTML.
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Messengers Insights</title>
	
<style>
* { 
	margin:0;
	padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

body {
	-webkit-font-smoothing:antialiased; 
	-webkit-text-size-adjust:none; 
	width: 100%!important; 
	height: 100%;
}

.container {
	display:block!important;
	max-width:400px!important;
	margin:40px auto!important;
	clear:both!important;
}

.content {
	padding:15px;
	max-width:400px;
	margin:0 auto;
	display:block; 
}

p {
	margin: 40px auto;
}

</style>
</head>


<body bgcolor="#FFFFFF">
	<section class="container">


		<h3>Добро пожаловать!</h3>
		<p>
			Messengers Insights - это автоматизированный сервис для анализа мессенджеров. Система позволяет в онлайн-режиме получать рейтинги каналов и чатов в Telegram, аналитику канала или чата в Telegram, поиск по сообщениям в Telegram.
		</p>
		<p>
		Если вы не зарегистрировались на mes-ins.ru, не обращайте внимания на это письмо.

		</p>
		<p>

		На данный момент мы завершаем разработку системы и в ближайшее время вышлем отдельное письмо для регистрации и получения доступа в Messengers Insights.
		</p>
		<p>

		Спасибо!
		</p>


		<p>Контактная информация:</p>

		<div>Телефон: +7 (495) 649-93-20</div>
		<div>E-mail: contact@mes-ins.ru</div>

	</section>

</body>
</html>',
-- Тело в формате плоского текста.
'Добро пожаловать!

Messengers Insights - это автоматизированный сервис для анализа мессенджеров. Вы можете наблюдать динамику, сообщения и представления в режиме реального времени.

Это электронная почта активации учетной записи.

Если вы не зарегистрировались на mes-ins.ru, не обращайте внимания на это письмо.

Чтобы завершить регистрацию, пожалуйста, подтвердите свои намерения, нажав кнопку «Активировать учетную запись», после чего вы будете перенаправлены на сайт «Messengers Insights», чтобы указать пароль.

Активируйте учетную запись!


Контактная информация:

Телефон: +7 (495) 649-93-20
Email: contact@mes-ins.ru'
)

INSERT INTO "MailTemplates"."DispatchTypes" (template, title) VALUES
(2, 'GREETY');


-- Создание схемы для храенения шаблонов.
CREATE SCHEMA "MailTemplates";


CREATE TABLE "MailTemplates"."Templates"(
-- Общая таблица шаблонов.
id serial  UNIQUE NOT NULL PRIMARY KEY, -- уникальный идентификатор шаблона.
version varchar(255) UNIQUE, -- версия шаблона
description varchar(255) -- дополнительное описание шаблона
);

-- Создание шаблона-заглушки.
INSERT INTO "MailTemplates"."Templates" (version, description) VALUES
('init version 1.0', 'Init template, Use Default For Not Found Tamplates')

CREATE TABLE "MailTemplates"."Localization"(
-- Таблица языковых альтернатив.
template integer NOT NULL REFERENCES "MailTemplates"."Templates"(id), -- внешний ключ на версию шаблона.
lang varchar(50), -- языковая версия
subject varchar(77), -- заголовок письма
html_body text, -- тело письма в формате html
text_body text, -- тело письма в формате плоского текста
-- Ограничение на то чтобы у версии шаблона был только один возжможный вариант на каждом языке.
CONSTRAINT unique_template_localize_version UNIQUE (template, lang)
)

-- Создание заглушки на английском языке. Предполагается использование в случае отсутствия шаблона.
INSERT INTO "MailTemplates"."Localization" (template, lang, subject, html_body, text_body) VALUES
(1, -- Шаблон заглушка.
'ENG', -- Название языка.
'Not Found Template', -- Заголовок.
-- Тело в формате HTML.
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Messengers Insights</title>
</head>
<body bgcolor="#FFFFFF">
<table class="head-wrap" bgcolor="#333">
	<tr>
		<td></td>
		<td class="header container">
				<div class="content">
					<table bgcolor="#333">
					<tr>
						<td><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAAAyCAYAAAAZUZThAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjRDOTU0QUNFNzVBMTExRTc4NTRCRjA0MTdEQTcxODMzIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjRDOTU0QUNGNzVBMTExRTc4NTRCRjA0MTdEQTcxODMzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NEM5NTRBQ0M3NUExMTFFNzg1NEJGMDQxN0RBNzE4MzMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NEM5NTRBQ0Q3NUExMTFFNzg1NEJGMDQxN0RBNzE4MzMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6wbcXDAAAL80lEQVR42uxdCXRU5RW+M5NJJjuLVAggKaAsRhEKImIPnIqA2lK0UsFSrFKs1WotUouttrRaK9SWKmBF6UI9lUUquDSHpaKIIhSQQCHIUqBA2AmEkMxkmZneL+978ggQzBBmJsl/z/nOI/Pmf8u8+/13/R+ucDgsRowYObu4zU9gxIghiBEjhiBGjBiCGDESRUmIx4tKHfvOhR4iW/E7hUsxVrErWtdeeswvg3q2kYUP9jHaZSxI3EmaYoJiq+J2xW2KjYpnFMnmcRtpzAS5U7FF8QuF12mQFI8r8hXfNo/cSGMjyDWK9xWzFVnncbv+plih6G4evZGGTpC2ij8r1in61WIcgoNPFHPPQygjRuolQRIVP2Bscc8FHGcY3a5HGcwbMVLvCYLAe7NiiiKjDo6XqXhOsVPxdaMORuorQXIUuYp/KNpfhOO3UyxQLFZcbdTCSH0hCGb4FxQbFDdH4Xw3KfJooS6Jwf12UryrmC6n16g68zeYaFTWEARymeIhxf+4jWaM4GKMs4NbbxTPfZXiK4r7FDc4Pn+A++Biphq1jZ4kxOl1PS2xr1mk05IcVsyJ0jlTHP8eKVb6GtdxGz/zOp5ZR8Ut3L9IsYafXydWVs9Pa7RJ0U0xSFGuWCZW5g9yq6KnYrfidcVJjj+kCCqGKkoVCxV7OAbE/TLjto20tCsVAcWXFIMVxYp3OMlcoWjO6/smtwHeE+5lKZMlAUOQzy/xVPVOjOK5WnEbEivL9l3FEEUbx/4KRVe6gl7HhDJQUUmFswUEeVExy3Ef26jgkxUjFGFazbtJiMkk1AnFpRwDQvVQ/FDxh7NcdxMq/0zHZxNJYhDh1yRRX0423UkcyFHFb+PVfYxXF6sijq4lGMVzNeH2j7Qmo2lJVinmUcm7KP5EcrSmchcqponVZiO0LCDTeP4b45ClQ91nOF21ERzjpuL3IyEPcIJaLlataREVGvue5Lmg3L1oKUIk3AyxMowuJlV8it9wEvYorlR8h/eI8XcpmomVql9gYpA6CAww1ZUGKsVfVlknUYlLjxEMhaXUXyGVwZD+HfNyiG1B0BWAiv9LdFn+QtdI6BJ1UJRxZsdMn0HlX0kr8rbiW3Rn3uS4NxnboEjan58No8JPdJw/i4oPxd3L64BcQ1dpBq3QGpLWTbfLy7FHHGO6OqzQWFqYF/n3a4qfipU53GIIcoFSqYoM9e3fuYV0aZkupUUBwWrhSHUaZCgpKRePEmNATkvJapIsgYpgrG+zOberxeoSSCARZlDxhUE6mjIPKh7krAxFv0PxE7o1r4pV30EMkMvvv0wLs5MzN2QuY727aGlmcl+A8Yjt7tmukDgU3mnx/NzmkZij6RqOcngD+dy+RSLBSo4jocYbglyon6NsKC0PyshebeTZoVfKwO6tqxS65GS5YF29qxbECOhxSor80i27mTw6uJPcf307aZ3pk2BZZaxvs63jubzFQHcC3Tz74rD9iJk+21q0JMbQ4sAd+ljxRcXvGYzfL1YdCef4gMfqTKuTxxjDRYV2/pxJ3K5lfHM3Lc5kxhcgx4cM7K8icXHdSFln0kI548r7SKIHaIFA3ptMFqsOXKwEt0v2qeWY+0mBdLgkTfoN6Srz1hXIuu1HxZvsFW9CzXxXIySB435p1SJVht/YQS5NS5L5eftl0/5iScTY2LpYLmakjlDpgK859hdzi9n9Xrox8x3736YXOoSuGeQV+v9zHJk4kOQpxjjjeT5bkPE6pmhxlnhwNy0VXKvHmNXaxNhiLcmy1JEhgzzhmITLHS7iGAbmwvuYYghSRyrkS/TIkaIyWbxqj/TuliUjeraRvu2byazVe+XoMb/4UhPF43GJ/bIWF30Ev1oab5JHht3QTnq3ayrrCk7ItPd2SHlxQEbd2FGK/DGPP3DFd9KXt0Mup7zHTNUuKmcXWobLHLO7mxYEM/l2kgaK+Yait1h1JTsgfpz/Rlq3RKxULmKOfdU8i7/S5cN5j9PqJJOwy/mdplR0O/WcSiuF68qmldrksCAzGdjv4TWUGILUoRol+/SylQirth6WtTsLZUTvtvLzWzvLovxDkpunz9fjlhT9DgxCaVlQwhrY9+7SQm7v1koKSytk8rvbpeDgSfGmJ4lbrYiljXXz+qPEDJ8sWr9fFuq1DO76hdoOX1PDPrgwSxx/n5Qz6zMhxhy51T5fWi39a8sqwin/rvb3bgLyK2ay4BpdzWzUdMY1QoK9XG38LjlzRedHRNxL/SOIY2pNTUuU8oqQvPrBTnm/VbqM6tVWrs1uKnPX7pX8HYVVPlU7tRbDe7SWdCXMPHWnVm9Vj8KrBGriq7IylXUcl3v12OVFQXleSRgBQeJdQJ71igG0JshMvSANWBLq88VDwRF3YNbee7hUnsn9tCojNVKJslmJgRRuTqsMySsoktfX7JWwkilFrUbVwPBZnJi6uCYEOhlJsjb/oBRqvNOsSYNa6Yvg+5/Um5BEt0ZkCBI5UcKSkpJQlQpesn6ffLjtiIzumy0piW55bsk2KdTYJFGtjVetyMV+k2RVJFNWKS3VkqWkJTVEnQlLfBVyDUE+rzXxaNCRmu6TioqgTFu63bIw6vKkZvqqiBG1t6yWlMtjAzqKL8G8Vam+S4N7giBCgiqmT61FMlK/Xo9E8/3DJScC0qd7VpWbF4GM5Qx9bQRjZ3Fsdg3fQV0E6ePXzrEfYy8X8760hmdBznB1YlXTCIYlwxdxh7xdSW8awVikVVE4PFnDd9CThR6py2ogGeoayC4EDD0aMEHquUVHXQB1DtQcUOUeSfLgrSzopUKRD0U71BLW0yKgVoHM0gke4xtitbiDOAdIjIPcVyTWKkoU/NCOglRtKi0ItmhkRO8W2lzQyIimSKSJc+VUW4khiJGYCTJEj4hVVEOrht07hR6nHLpiDzu+n0nXaBTdqO9T+SFjuAVxvsfj9SXx7F4qrONA0a4l/0YfGIp5aF4cyM/QGoIu4PkmBjESa6l0WIL/iNU/NVWsviV02aKXaR9dMRDmFbpPSLuis3YcLUtzkiFElHBSzOTxQKr/0mIsJGkqOGYLyYHuW7SroF9qhQnS40O8cXQtnhidN51bWBJUqO22Dsz0KM5lUYlz6DrhN0NbSVc+1+dpLeyeLdtaZNAdQ0Uc7Sgr+TlWB+6nW7WC5/iUlmMZz3HQECQ+JJ783PIYTxIF1Z4VYgm0fKD7Fv1Ns2kNsDQ2zDgE0tNxrPZyelnUeU+l3CbTCqXS8thx0D102zYwFjJBehzIE5zlnqI7EAtBMx4W9LxRq1HhsJRH3jYfdpDD5VBcJ2FgCSaI1bz4Y/5OCNa3MXj/2DHzg1TISHWndTjbhJjsCNx38rxoZ1/FJMEksdado/u2k7Eg8SFojpvCTMsUuShNITUq6VTOulOlFlXjqov0uKVNi4hfPGKTokJOLVSyt3ZbBw7+VbFe6LCGY37ksBz5jB2g7FgD0o0uWso5fkf7/mA9nmZsM5qJACykOkBygGBzGhtB4j2LVcRsDTpEJ+nkfLPbxWUbdUQZa53JZ/PEEga4GyKKrINh8aYlypODroj0cqbT39/MWRxp3cPc9y8q/nrGD1j3gaW3ixjIQ4H/zuD+OGMTPy2I/dYQxBbXyek1jmfFShOX8FyIYfrwmIhH7uCvjQ7eo4Yg8SkblRy3JLhdQ0NhmaSKeHldvSkrpAesCIa2pHgTHpZQeHGM73OXnGoNL6bbZEv1dvfq6dbNhDBAx8SC9SDXi5Umxv+RckROXyAF2Uo4J6WFjr9fkkYs9SrNm+BxLSgOVOb4K4IPqRk5ccHWw6XKUBkat/eYv4vH7VocXU/uospyxhC9mOmCq/QzMdJgLYhTqctdLhdiA1R6fymRv+EdbRWPqCU65Fa/LfzZ66EahMyTU28cuUiN/caCxLugHnAvMzTLajEOWZ4enFUPNfDnGzLkaLwEsQXV3/5i9R7tq+F7dsUYPvk68+iNNBaC2IJ6RSe6Xc7ULLIzeMMfXmQw2zxyIw06BjmPoNV7gliv5kTuHkv6UCPYZR61kYhi3mguJjJixLhYRowYghgxYghixIghiPkJjBg5t/xfgAEAERBBpeRAzN4AAAAASUVORK5CYII=" /></td>
					</tr>
				</table>
				</div>
		</td>
		<td></td>
	</tr>
</table>
<table class="body-wrap">
	<tr>
		<td></td>
		<td class="container" bgcolor="#FFFFFF">
			<div class="content">
			<table>
				<tr>
					<td>
						<h1>Template NOT FOUND!</h1>
                    </td>
                </tr>
                <tr>
					<td>
						<h2>Телефон: +7 (495) 649-93-20</h2>
                    </td>
                </tr>
                <tr>
					<td>
						<h2>Email: contact@mes-ins.ru</h2>
                    </td>
                </tr>
            </table>
            </div>
        </td>
    </tr>
</table>
</body>
</html>',
-- Тело в формате плоского текста.
'    Template Not Found.

Телефон: +7 (495) 649-93-20
Email: contact@mes-ins.ru'
)



CREATE TABLE "MailTemplates"."DispatchTypes"(
-- Таблица типов уведомлений.
template integer NOT NULL REFERENCES "MailTemplates"."Templates"(id) DEFAULT 1, -- внешний ключ на шаблон.
id serial UNIQUE NOT NULL PRIMARY KEY, -- номер типа уведомления.
title varchar(100) UNIQUE NOT NULL, -- Название типа уведомления.
description varchar(255) -- описание типа опционально.
);

INSERT INTO "MailTemplates"."DispatchTypes" (template, title) VALUES
(1, 'EMPTY');

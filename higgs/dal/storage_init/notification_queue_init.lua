-- Базовая конфигурация
box.cfg {
    listen = 3301,
    slab_alloc_arena = 2,
    log_level = 5,
    work_dir = '/opt/tarantool'
}

-- Команды инициализации очереди тарантула.
queue = require 'queue'

-- Создание трубы для очереди уведомлений.
queue.create_tube('notification_mail', 'fifottl', {
    temporary = false,
    if_not_exists = true,
})

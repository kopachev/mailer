"""
Менеджер взаимодействия с сервером TARANTOOL
"""
import asyncio
import logging

import asynctnt
import asynctnt_queue
from asynctnt.exceptions import TarantoolError
from asynctnt_queue.exceptions import QueueError, TaskEmptyError

from higgs.config import APP_NAME, TARANTOOL
from higgs.config.database import NOTIFICATIONS_TUBE

log = logging.getLogger(APP_NAME + ".tr_manager")


class TarantoolQueueManager:
    """
    Менеджер глобального контекста управления подключением к тарантулу
    Создает обьект соединения очереди и трубы. Содержит методы доступа к данным
    """

    # TODO написать логгеры создания пула и инициализации соединений
    def __init__(self, connection, tubes, loop=None):
        self._loop = loop or asyncio.get_event_loop()
        self._connection = connection
        self._queue = asynctnt_queue.Queue(connection)
        self._tubes = tubes
        self._notifications_tube = self._queue.tube(self._tubes[NOTIFICATIONS_TUBE])
        self.task_empty_exc = TaskEmptyError
        self.database_exc = TarantoolError

    async def make_notification_mail_task(self, task, ttr=None):
        """
        Выпонение запроса на добавление задачи на рассылку уведомлений.
        Добавляет задачу в очередь для рассылки уведомлений.
        """
        tube = self._queue.tube(self._tubes[NOTIFICATIONS_TUBE])
        try:
            task_info = await tube.put(task, ttr=ttr)
        except QueueError:
            log.exception(
                'Error when put task to tube: %s', self._tubes[NOTIFICATIONS_TUBE]
            )
            raise
        return task_info

    async def get_notification_tube_statistic(self):
        """
        Выпонение запроса на добавление электронный почты для уведомления.
        Получение статистики очереди уведомлений.
        """
        stats = await self._queue.statistics(self._tubes[NOTIFICATIONS_TUBE])
        return stats

    async def get_notification_task(self, timeout=None):
        """
        Получение задачи из очереди почтовых уведомлений.
        Асихронное выполнение.
        """
        try:
            task = await self._notifications_tube.take(timeout)
        except TaskEmptyError:
            raise
        except (QueueError, TarantoolError):
            log.exception(
                'Error when try get task from tube: %s', self._notifications_tube
            )
            return None
        return task

    async def task_release(self, task, delay=None):
        """
        Возвращение задачи в очередь
        """
        try:
            await task.release(delay)
        except (QueueError, TarantoolError):
            log.exception(
                'Error when try get task from tube: %s', task.tube
            )
        return task

    async def ack_task(self, task):
        """
        Асинхронное удаление задачи из очереди.
        """
        try:
            task = await task.ack()
        except (QueueError, TarantoolError):
            log.exception(
                'Error acked task %s in tube: %s', task, task.tube
            )
        return task

    async def bury_task(self, task):
        """
        Асинхронное архивирование задачи из очереди.
        """
        try:
            task = await task.bury()
        except (QueueError, TarantoolError):
            log.exception(
                'Error bury task %s in tube: %s', task, task.tube
            )
        return task

    @classmethod
    async def create_manager(cls):
        """
        Асихронное создание менеджера Очередей.
        """
        tubes = TARANTOOL['tubes']
        conn = asynctnt.Connection(**TARANTOOL['config'])
        log.info(
            'Try connect to Tarantool server %s post %s', TARANTOOL['config']['host'], TARANTOOL['config']['port']
        )
        await conn.connect()
        return cls(conn, tubes)

    async def connection_close(self):
        """
        Метод для закрытия пула соединений.
        """
        await self._connection.disconnect()

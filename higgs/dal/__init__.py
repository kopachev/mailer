"""
Работа с базами данных.
"""
import asyncio

from .tarantool import TarantoolQueueManager
from .postgres import PostgresManager
from .redis import RedisManager
from higgs import config
import logging

log = logging.getLogger(config.APP_NAME + '.dbases')


async def init_pg():
    """
    Создает асинхронный пул соединений
    """
    pg_db = await PostgresManager.create_manager()
    return pg_db


async def close_pg(pg_db):
    """
    Закрывает пул соединений
    """
    await pg_db.pool_close()


async def init_tarantool(loop=None):
    """
    Создает менеджера для управления очередями.
    """
    tr_qm = await TarantoolQueueManager.create_manager()
    return tr_qm


def sync_tarantool_init(loop=None):
    """
    Синхронное подключение к бд тарантул.
    """
    if loop is None:
        loop = asyncio.get_event_loop()
    tr_qm = loop.run_until_complete(init_tarantool())
    return tr_qm


async def close_tarantool(tr_qm):
    """
    Закрывает соединение с очередью.
    """
    await tr_qm.connection_close()


async def init_redis():
    """
    Создает асинхронный пул соединений с БД редис
    """
    redis_db = await RedisManager.create_manager()
    return redis_db


async def common_db_init(loop=None):
    """
    Общая инициализация dal
    """
    pg_db = await init_pg()
    tr_qm = await init_tarantool()
    redis_db = await init_redis()
    db_cage = {
        'pg_db': pg_db,
        'tr_qm': tr_qm,
        'redis': redis_db
    }
    return db_cage


async def common_db_closing(db_cage):
    """
    Общее закрытие подключений.
    """
    await db_cage['pg_db'].pool_close() if db_cage.get('pg_db', None) else None
    await db_cage['tr_qm'].connection_close() if db_cage.get('tr_qm', None) else None

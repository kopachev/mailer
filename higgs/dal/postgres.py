"""
Выполнение запросов к СУБД Postgres
"""
import logging

import asyncpg
from higgs.config import APP_NAME, POSTGRES

log = logging.getLogger(APP_NAME + ".pg_manager")


class PostgresManager:
    """
    Менеджер глобального контекста
    Управляет подключением.
    Содержит методы взаимодействия с БД.
    """

    DUPLICATE_DATA = asyncpg.UniqueViolationError

    # TODO написать логгеры создания пула и инициализации соединений
    def __init__(self, pool):
        self._pool = pool

    async def add_subscriber_mail(self, client_email, ip_address=None, language=None):
        """
        Выпонение запроса на добавление электронный почты для уведомления.
        Возврещает данные о созданной записи в виде словаря.
        """
        query_string = 'INSERT INTO public."Subscriptions" (email, ip_address, lang) ' \
                       'VALUES ($1,  $2, $3) RETURNING subscription_id, email;'
        async with self._pool.acquire() as conn:
            try:
                record = await conn.fetch(query_string, client_email, ip_address, language)
            except Exception:
                log.debug('Error in email saving. email.: %s ip: %s', client_email, ip_address)
                raise
            else:
                log.debug('Email %s from ip: %s, was saved', client_email, ip_address)
        return dict(record)

    async def get_user_by_id(self, user_id: int):
        """
        Получение пользователя по id.
        Возвращает данные пользователя в виде словаря.
        Из базы ожидается список из одного пользователя или пустой список.
        """
        query_string = 'SELECT * FROM public."Users" WHERE user_id=$1;'
        async with self._pool.acquire() as conn:
            try:
                records = await conn.fetch(query_string, user_id)  # список из одного пользователя
            except Exception:
                log.debug('Can not get user with id %s', user_id)
                raise
            else:
                log.debug('Query on getting user user_id %s successful. result %s', user_id, records)
        if records is None or not isinstance(records, list):
            reason = 'Bad response (records is None) from database!'
            log.warning(reason)
            raise asyncpg.PostgresError(reason)
        user_record = next(iter(records), None)
        if user_record is not None:
            user = dict(user_record)
            log.debug(
                'Get user id: %s'.format(user.get('user_id', None))
            )
        else:
            log.warning(
                'User by user id: %s not found', user_id
            )
            user = None
        return user

    async def get_all_users(self, user_id=None):
        """
        Получение пользователя по id.
        Возвращает итератор преобразовывающий записи о пользователях в словари.
        """
        query_string = 'SELECT * FROM public."Users";'
        async with self._pool.acquire() as conn:
            try:
                records = await conn.fetch(query_string)  # получаем итератор
            except Exception:
                log.debug('Can not get user with id %s', user_id)
                raise
            else:
                log.debug('Query on getting users successful. result %s', records)
        if records is None or not isinstance(records, list):
            reason = 'Bad response (records is None) from database!'
            log.warning(reason)
            raise asyncpg.PostgresError(reason)
        log.debug(
            '%s users found', len(records)
        )
        users = map(lambda x: dict(x), records or [])
        return users

    async def get_mail_content(self, email_type, lang):
        """
        Метод получения тектса шаблона письма.
        """
        query_string = 'SELECT * FROM "MailTemplates"."Localization" ' \
                       'JOIN "MailTemplates"."DispatchTypes" USING (template) ' \
                       'WHERE title=$1 and lang=$2 LIMIT 1'

        async with self._pool.acquire() as conn:
            try:
                records = await conn.fetch(query_string, email_type, lang)  # список из одного пользователя
            except Exception:
                log.debug('Can not get template type %s, lang %s', email_type, lang)
                raise
            else:
                log.debug('Query on template type %s, lang %s success', email_type, lang)
        if records is None or not isinstance(records, list):
            reason = 'Bad response (records is None) from database!'
            log.warning(reason)
            raise asyncpg.PostgresError(reason)
        template_record = next(iter(records), None)
        if template_record is not None:
            template = dict(template_record)
            log.debug(
                'Get template type: %s with subject: %s', template.get('title', None), template.get('subject', None)
            )
        else:
            log.warning(
                'Template type %s on lang %s  not found', email_type, lang
            )
            template = None
        return template

    @classmethod
    async def create_manager(cls):
        """
        Асихронное создание менеджера СУБД
        """
        try:
            manager = cls(await asyncpg.create_pool(**POSTGRES))
        except (asyncpg.PostgresError, ConnectionRefusedError):
            log.exception(
                "Can't connect to Postgres server %s post %s", POSTGRES['host'], POSTGRES['port']
            )
            raise

        log.info(
            'Connected to Postgres server %s post %s', POSTGRES['host'], POSTGRES['port']
        )
        return manager

    async def pool_close(self):
        """
        Метод для закрытия пула соединений
        """
        await self._pool.close()
        log.info(
            'Postgres connection pool closed server %s post %s', POSTGRES['host'], POSTGRES['port']
        )

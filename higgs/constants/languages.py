"""
Языки
"""
import enum


class Language(enum.Enum):
    """
    Языки доступные в системе.
    """
    EN = 'ENG'
    RU = 'RUS'

## Notification processing service

Сервис предназначен для получения уведомлений о событиях, формировании и выполнении задач по рассылке элетронных писем.  


### Основные технологии:

- Python 3.6.
- Асинхронный сервер на базе aiohttp.  
- asyncpg - асинхронная библиотека для работы с СУБД PostgreSQL.  
- asynctnt-queue - асинхронная библиотека для работы с очередями tarantool.  
- aredis - асихронная работа с хранилищем REDIS.
- grpcio - обеспечение взаимодействия через Protocol Buffers.

Переменные окружения
====================


|Переменная|Значение по умолчанию|Описание|  
|-|-|-|
|SENTRY_DSN||DSN для Sentry|
|APP_SECRET_KEY|you-will-never-guess-mi|секретный ключ веб приложения|
|APP_DEBUG|FALSE|Переменная для обозначения среды разработки|
|APP_HOST|127.0.0.1|Адрес привязки сервера|
|APP_PORT|8881|Порт сервера|
|ALLOW_DOMAINS|*| Разрешенные домены(список строк) |
|ME_POSTGRES_HOST|localhost|Хост для основной БД (PostgreSQL)|
|ME_POSTGRES_PORT|5432|Порт для основной БД (PostgreSQL)|
|ME_POSTGRES_USER|auth|Пользователь для основной БД (PostgreSQL)|
|ME_POSTGRES_PASS|auth|Пароль для основной БД (PostgreSQL)|
|ME_POSTGRES_DB|auth|Имя базы для основной БД (PostgreSQL)|
|ME_TARANTOOL_HOST|localhost| Сервер очередей tarantool|
|ME_TARANTOOL_PORT|3301| Порт сервера очередей tarantool|
|ME_TARANTOOL_USER|local| Пользователь для работы с очередью|
|ME_TARANTOOL_PASS|local| Пароль пользователя очереди|
|ME_TARANTOOL_TUBE|notification| Название очереди|
|ME_REDIS_HOST|127.0.0.1| Адрес хранилища REDIS|
|ME_REDIS_PORT|6379| Порт хранилища|
|ME_REDIS_PASS|local| Пароль на доступ к хранилищу|
|ME_REDIS_DB|5| Номер базы данных |


### Компиляция прото.
Действия выполняются из корня проекта.

- Подключить submodule protobuf.

```bash
mkdir proto
git submodule add https://git.d-ops.net/messenger-ear/protobuf.git proto
```

- Запустить скрипт.

```bash
sh proto_compile.sh

```